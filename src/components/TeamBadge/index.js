import React from "react";
import { View, StyleSheet, Image, Text } from "react-native";
import theme from "tlc/theme";
import { aspectSizer } from "utils";

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    backgroundColor: theme.primaryColor,
    justifyContent: "space-between",
    padding: 10,
    borderRadius: 100,
    alignItems: "center",
    width: "50%"
  },
  teamName: {
    fontFamily: theme.boldFont,
    fontSize: aspectSizer(12),
    color: theme.lightTextColor,
    textTransform: "uppercase",
    marginLeft: 10,
    flexShrink: 1
  },
  image: {
    width: aspectSizer(50),
    height: aspectSizer(50),
    alignSelf: "center",
    backgroundColor: "#fff",
    borderRadius: 150,
    overflow: "hidden"
  }
});

const TeamBadge = ({ team: { team_badge, team_name } }) => (
  <View style={styles.container}>
    <Image style={styles.image} source={{ uri: team_badge }} />
    <Text style={styles.teamName}>{team_name}</Text>
  </View>
);

export default TeamBadge;

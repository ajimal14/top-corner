import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import EventPicker from "screens/EventPicker";
import EventDetails from "screens/EventDetails";
import ChampionsLeague from "screens/ChampionsLeague";

import theme from "tlc/theme";

const Event = createStackNavigator();

const EventStack = () => (
  <Event.Navigator
    initialRouteName="Events"
    screenOptions={{
      headerStyle: {
        backgroundColor: theme.secondaryColor
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontFamily: theme.boldFont,
        color: "#fff",
        textTransform: "uppercase"
      }
    }}
  >
    <Event.Screen name="Events" component={EventPicker} />
    <Event.Screen name="Event Details" component={EventDetails} />
    <Event.Screen
      name="Champions League"
      component={ChampionsLeague}
      options={{
        headerStyle: {
          backgroundColor: theme.clBlue
        },
        tabBarVisible: false
      }}
    />
  </Event.Navigator>
);

export default EventStack;

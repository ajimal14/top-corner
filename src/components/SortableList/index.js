import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  SafeAreaView,
  FlatList,
  TouchableNativeFeedback,
  TouchableOpacity,
  ScrollView
} from "react-native";

import EmptyState from "components/EmptyState";

import { Ionicons } from "@expo/vector-icons";

import { connect } from "react-redux";

import {
  selectData,
  setSorting,
  initTable,
  selectActiveSort,
  setSearchValue,
  selectSearch
} from "store/sortablePlayerList";

import { debounce, aspectSizer } from "utils";
import theme from "tlc/theme";

const styles = StyleSheet.create({
  container: {
    flex: 1
  },

  header: {
    flexDirection: "row",
    justifyContent: "space-around",
    flexWrap: "wrap",
    backgroundColor: "#e3e3e3",
    paddingVertical: 10,
    borderRadius: 50
  },
  filters: {
    flexDirection: "row",
    flexWrap: "wrap"
  },
  chip: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: theme.primaryColor,
    borderRadius: 30,
    marginRight: 10,
    marginVertical: 10,
    color: theme.lightTextColor,
    fontFamily: theme.boldFont,
    fontSize: 10
  },
  activeChip: {
    backgroundColor: theme.secondaryColor,
    color: theme.lightTextColor
  }
});

class SortableList extends React.PureComponent {
  state = {
    currentFilter: null
  };
  componentDidMount() {
    const { list, initTable } = this.props;
    initTable(list);
  }

  componentWillUnmount() {
    const { setSorting, setSearchValue } = this.props;
    setSorting({
      key: null,
      direction: "",
      type: null
    });
    setSearchValue("");
  }

  handleSortClick = header => {
    const {
      sort: { key, direction },
      setSorting
    } = this.props;

    if (header.key) {
      let sortDirection = "asc";
      if (key === header.key) {
        if (direction == "") {
          sortDirection = "asc";
        } else if (direction === "asc") {
          sortDirection = "desc";
        } else if (direction === "desc") {
          sortDirection = "asc";
        }
      }

      let data = {
        key: header.key,
        direction: sortDirection,
        type: header.type
      };

      setSorting(data);
    }
  };

  renderSortArrow = sortKey => {
    const {
      sort: { key, direction }
    } = this.props;

    return (
      <Text
        style={{
          marginLeft: 5,
          alignItems: "center"
        }}
      >
        <Ionicons
          name="md-arrow-dropup"
          size={12}
          color={
            sortKey === key && direction === "asc"
              ? theme.secondaryColor
              : "grey"
          }
        />
        <Ionicons
          name="md-arrow-dropdown"
          size={12}
          color={
            sortKey === key && direction === "desc"
              ? theme.secondaryColor
              : "grey"
          }
        />
      </Text>
    );
  };

  renderHeaders = () => {
    const { headers } = this.props;
    return (
      <View style={styles.header}>
        {headers.map(header => (
          <TouchableOpacity
            onPress={() => this.handleSortClick(header)}
            key={header.key}
            style={[
              header.style,
              { flexDirection: "row", alignItems: "center" }
            ]}
          >
            <Text
              style={{
                fontFamily: theme.boldFont,
                fontSize: aspectSizer(12),
                color: "grey"
              }}
            >
              {header.name}
            </Text>
            {this.renderSortArrow(header.key)}
          </TouchableOpacity>
        ))}
      </View>
    );
  };

  renderContent = () => {
    const { data, component } = this.props;
    const { currentFilter } = this.state;
    let list = data;
    const Comp = component;

    if (currentFilter) {
      list = data.filter(player => currentFilter.filter(player));
    }

    if (list && list.length > 0) {
      return (
        <FlatList
          data={list}
          renderItem={item => <Comp {...item} />}
          keyExtractor={item => item.player_key}
        />
      );
    } else return <EmptyState />;
  };

  renderSearch = () => {
    const { setSearchValue, search } = this.props;
    return (
      <View
        style={{
          marginVertical: 10
        }}
      >
        <TextInput
          style={{
            height: 40,
            borderColor: theme.borderColor,
            borderWidth: 1,
            width: "70%",
            borderRadius: 30,
            paddingHorizontal: 15,
            alignSelf: "flex-end",
            fontFamily: theme.boldFont
          }}
          onChangeText={text => debounce(setSearchValue(text), 1500)}
          value={search}
          placeholder="Search Player"
        />
      </View>
    );
  };

  handleFilterClick = filter => {
    const { currentFilter } = this.state;
    if (currentFilter && currentFilter.name === filter.name) {
      this.setState({ currentFilter: null });
    } else {
      this.setState({ currentFilter: filter });
    }
  };

  renderFilters = () => {
    const { filters } = this.props;
    const { currentFilter } = this.state;
    if (filters) {
      return (
        <View style={styles.filters}>
          {filters.map(filter => (
            <TouchableNativeFeedback
              onPress={() => this.handleFilterClick(filter)}
              key={filter.name}
            >
              <Text
                style={[
                  styles.chip,
                  currentFilter &&
                    currentFilter.name === filter.name &&
                    styles.activeChip
                ]}
              >
                {filter.name}
              </Text>
            </TouchableNativeFeedback>
          ))}
        </View>
      );
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.renderSearch()}
        {this.renderFilters()}
        {this.renderHeaders()}
        {this.renderContent()}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  data: selectData(state),
  sort: selectActiveSort(state),
  search: selectSearch(state)
});

const mapDispatchToProps = dispatch => ({
  initTable: data => dispatch(initTable(data)),
  setSorting: data => dispatch(setSorting(data)),
  setSearchValue: data => dispatch(setSearchValue(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SortableList);

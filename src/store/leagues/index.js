import reducerRegistry from "../reducerRegistry";

import { SportsAPI, createActionName } from "../utils";
import { topLeagues } from "../utils/initialData/leagues";

const initialState = {
  error: false,
  loading: false,
  errorData: {},
  data: {},
  selected: null,
};

export const reducerName = "getLeagueList";

const url = "?action=get_leagues";

export const getLeagueListData = countryID => {
  return SportsAPI.get(url + `&country_id=${countryID}`)
    .then(resopnse => {
      return resopnse;
    })
    .catch(error => {
      if (error.response) {
        return error.response;
      } else if (error.request) {
        return error.request;
      } else {
        return { status: 400, message: error.message };
      }
    });
};

// SELECTORS //
export const selectData = state => {
  return state[reducerName] ? state[reducerName].data : initialState.data;
};
export const selectActiveLeague = state => {
  return state[reducerName]
    ? state[reducerName].selected
    : initialState.selected;
};
export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;
export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const GET_LEAGUE_LOADING = createActionName(reducerName, "LOADING");
export const GET_LEAGUE_SUCCESS = createActionName(reducerName, "SUCCESS");
export const GET_LEAGUE_ERROR = createActionName(reducerName, "ERROR");
export const SET_ACTIVE_LEAGUE = createActionName(reducerName, "ACTIVE");

// ACTION CREATORS //
export const getLeagueListLoading = () => {
  return {
    type: GET_LEAGUE_LOADING
  };
};

export const getLeagueListSuccess = payload => {
  return {
    type: GET_LEAGUE_SUCCESS,
    payload
  };
};

export const getLeagueListError = error => {
  return {
    type: GET_LEAGUE_ERROR,
    error
  };
};

export const setActiveLeague = payload => {
  return {
    type: SET_ACTIVE_LEAGUE,
    payload
  };
};

export const getLeagueList = countryID => dispatch => {
  dispatch(getLeagueListLoading());
  getLeagueListData(countryID).then(response => {
    if (response.status > 0 && response.status < 400) {
      const data = { [countryID]: response.data };
      dispatch(getLeagueListSuccess(data));
    } else {
      dispatch(getLeagueListError(response));
    }
  });
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case GET_LEAGUE_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_LEAGUE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        data: { ...state.data, ...action.payload }
      };
    case GET_LEAGUE_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.error
      };
    case SET_ACTIVE_LEAGUE:
      return {
        ...state,
        selected: action.payload
      };
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

import React from "react";
import {
  TouchableNativeFeedback,
  View,
  Image,
  Text,
  StyleSheet
} from "react-native";
import theme from "tlc/theme";
import { aspectSizer } from "utils";

const styles = StyleSheet.create({
  container: {
    padding: 5,
    backgroundColor: "#fff",
    alignItems: "center",
    width: "30%",
    alignSelf: "auto",
    marginLeft: "auto",
    marginRight: "auto",
    marginBottom: 10
  },
  title: {
    color: theme.primaryTextColor,
    fontSize: aspectSizer(12),
    fontFamily: theme.boldFont,
    textTransform: "uppercase",
    marginVertical: 10,
    backgroundColor: '#e3e3e3',
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 50
  },
  image: {
    width: aspectSizer(50),
    height: aspectSizer(50),
    alignSelf: "center",
    marginVertical: 10
  }
});

const CountryCard = ({ item, onPress }) => (
  <TouchableNativeFeedback
    background={TouchableNativeFeedback.Ripple("#e3e3e3")}
    onPress={() => onPress(item)}
  >
    <View style={styles.container}>
      {item.badge && (
        <Image
          style={styles.image}
          source={{ uri: item.badge }}
          resizeMode="contain"
        />
      )}
      <Text style={styles.title}>{item.country_name}</Text>
    </View>
  </TouchableNativeFeedback>
);

export default CountryCard;

import React from "react";
import { StyleSheet, FlatList, Text, View, SafeAreaView } from "react-native";
import { connect } from "react-redux";

import {
  getLeagueList,
  selectErrorStatus,
  selectLoadingStatus,
  selectData,
  setActiveLeague,
  selectActiveLeague,
  getLeagueListSuccess
} from "store/leagues";

import Loader from "components/Loader";
import FadedHeading from "components/FadedHeading";
import LeagueCard from "components/Cards/LeagueCard";
import PrimaryButton from "components/Buttons/PrimaryButton";
import { topLeagues } from "store/utils/initialData/leagues";

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

class LeaguePicker extends React.Component {
  componentDidMount() {
    const {
      route: {
        params: { countryID }
      },
      getLeagueListSuccess
    } = this.props;

    if (!topLeagues.hasOwnProperty(countryID)) {
      this.loadAllLeagues();
    } else {
      getLeagueListSuccess({ [countryID]: topLeagues[countryID] });
    }
  }

  loadAllLeagues = () => {
    const {
      getLeagueList,
      route: {
        params: { countryID }
      }
    } = this.props;
    getLeagueList(countryID);
  };

  handleClick = ({ league_id }) => {
    const { navigation, setActiveLeague } = this.props;
    setActiveLeague(league_id);
    navigation.navigate("Team", {
      leagueID: league_id
    });
  };

  renderContent = () => {
    const {
      loading,
      data,
      route: {
        params: { countryID }
      }
    } = this.props;

    const leagues = data && data[countryID] || [];

    if (loading) {
      return <Loader />;
    } else if (leagues.length > 0) {
      return (
        <FlatList
          contentContainerStyle={{
            marginHorizontal: "auto",
            marginVertical: 10,
            alignSelf: "center"
          }}
          data={leagues}
          renderItem={item => (
            <LeagueCard
              {...item}
              onPress={league => this.handleClick(league)}
            />
          )}
          keyExtractor={item => item.league_id}
          numColumns={3}
        />
      );
    } else {
      return <Text style={styles.container}>No Competetion Data</Text>;
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <FadedHeading>Pick League</FadedHeading>
        <View style={{
          flex: 1
        }}>{this.renderContent()}</View>
        <PrimaryButton onPress={this.loadAllLeagues}>
          Load All Leagues
        </PrimaryButton>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  loading: selectLoadingStatus(state),
  data: selectData(state),
  error: selectErrorStatus(state),
  active: selectActiveLeague(state)
});

const mapDispatchToProps = dispatch => ({
  getLeagueList: countryID => dispatch(getLeagueList(countryID)),
  setActiveLeague: id => dispatch(setActiveLeague(id)),
  getLeagueListSuccess: data => dispatch(getLeagueListSuccess(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LeaguePicker);

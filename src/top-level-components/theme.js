const theme = {
  primaryColor: "#4CAF50",
  primaryColorLight: "#C8E6C9",
  lightTextColor: "#e8eaf6",
  boldFont: "raleway-bold",
  regFont: "raleway",
  secondaryColor: "#546e7a",
  secondaryColorLight: "#90a4ae",
  titleColor: "grey",
  primaryTextColor: "#757575",
  secondaryTextColor: "#757575",
  borderColor: "#e3e3e3",
  clBlue: "#0b096a"
};

export default theme;

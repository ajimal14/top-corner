import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";

import ChampionsLeague from "screens/ChampionsLeague";

const Champion = createStackNavigator();

const CL = () => (
  <NavigationContainer>
    <Champion.Navigator
      options={{
        tabBarVisible: false
      }}
      style={{
        positon: "absolute"
      }}
    >
      <Champion.Screen
        name="Champions League"
        component={ChampionsLeague}
        options={{
          headerStyle: {
            backgroundColor: "#0b096a"
          },
          headerTitleStyle: {
            color: "#fff"
          }
        }}
      />
    </Champion.Navigator>
  </NavigationContainer>
);

export default CL;

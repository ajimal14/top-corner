import React from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import {
  Ionicons,
  MaterialCommunityIcons,
  MaterialIcons
} from "@expo/vector-icons";
import { aspectSizer } from 'utils';
import theme from "tlc/theme";

const styles = StyleSheet.create({
  container: {
    margin: 10
  },
  content: {
    backgroundColor: "rgba(0,0,0,0.2)",
    padding: 10,
    borderRadius: 10
  },
  logo: {
    width: aspectSizer(70),
    height: aspectSizer(70),
    overflow: "hidden",
    marginBottom: 20,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 50
  },
  matchdate: {
    color: "#fff",
    fontSize: aspectSizer(15),
    fontFamily: theme.regFont,
    textTransform: "uppercase",
    textAlign: "left",
    paddingLeft: 10
  },
  teamName: {
    fontFamily: theme.boldFont,
    color: "#fff",
    textTransform: "uppercase",
    textAlign: "center",
    marginBottom: 10
  },
  winner: {
    shadowColor: "#fff",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 1,
    shadowRadius: 1,
    elevation: 2,
    borderRadius: 50,
    marginBottom: 20,
    height: 60,
    alignItems: "center",
    justifyContent: "center",
    width: "70%",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 10,
    overflow:'hidden'
  }
});

class JourneyCard extends React.Component {
  renderGoals = side => {
    const { item } = this.props;
    const goals = item[`${side}_scorer`];
    if (goals.length > 0) {
      return (
        <LinearGradient
          colors={["rgba(0,0,0,0.1)", "rgba(0,0,0,0.5)", "rgba(0,0,0,0.1)"]}
          style={{
            paddingVertical: 10,
            paddingHorizontal: 20,
            alignSelf: "center",
            borderRadius: 5
          }}
          start={[1, 1]}
        >
          {goals.map((goal, index) => {
            return (
              <View
                key={"key" + index + side + item.venue + item.date}
                style={{}}
              >
                <Text
                  style={{
                    color: "#fff",
                    fontFamily: theme.regFont,
                    fontSize: aspectSizer(15)
                  }}
                >
                  <Ionicons name="ios-football" color="#fff" /> {goal.trim()}
                </Text>
              </View>
            );
          })}
        </LinearGradient>
      );
    } else return <View />;
  };

  renderTimeline = side => {
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between"
        }}
      >
        {this.renderGoals("home")}
        {this.renderGoals("away")}
      </View>
    );
  };

  renderScore = () => {
    const {
      item: { score }
    } = this.props;

    return (
      <LinearGradient
        colors={["rgba(0,0,0,0.1)", "rgba(0,0,0,0.5)", "rgba(0,0,0,0.1)"]}
        style={{
          padding: 10,
          alignItems: "center",
          borderRadius: 0,
          width: "30%",
          alignSelf: "flex-start"
        }}
        start={[1, 1]}
      >
        <Text
          style={{
            fontFamily: theme.boldFont,
            fontSize: aspectSizer(40),
            color: "#fff",
            borderBottomWidth: 0
          }}
        >
          {score}
        </Text>
      </LinearGradient>
    );
  };

  renderSide = side => {
    const { item } = this.props;

    return (
      <View
        style={{
          width: "40%",
          alignItems: "center"
        }}
      >
        <View style={styles.logo}>
          <Image
            style={{
              width: aspectSizer(50),
              height: aspectSizer(50)
            }}
            source={{ uri: item[`${side}_team_logo`] }}
          />
        </View>
        <Text style={styles.teamName}>{item[`${side}_team`]}</Text>
      </View>
    );
  };

  renderHead = () => (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "flex-start",
        marginVertical: 20
      }}
    >
      {this.renderSide("home")}
      {this.renderScore()}
      {this.renderSide("away")}
    </View>
  );

  renderDetails = () => {
    const {
      item: { venue, date }
    } = this.props;
    return (
      <View
        style={{
          marginTop: 10,
          alignSelf: "flex-start"
        }}
      >
        <View
          style={{
            flexDirection: "row"
          }}
        >
          <MaterialCommunityIcons name="stadium" size={15} color="#fff" />
          <Text style={styles.matchdate}>{venue}</Text>
        </View>
        <View
          style={{
            flexDirection: "row"
          }}
        >
          <MaterialIcons name="date-range" size={15} color="#fff" />
          <Text style={styles.matchdate}>{date}</Text>
        </View>
      </View>
    );
  };

  renderWinner = () => {
    const { year, item } = this.props;
    return (
      <LinearGradient
        colors={["rgba(255,22,255,0.5)", "rgba(90,247,220,0.5)"]}
        style={styles.winner}
        start={[1, 1]}
      >
        <Image
          source={{ uri: "https://i.imgur.com/kvSFvyu.png" }}
          style={{
            width: 50,
            height: 50
          }}
        />
        <Text
          style={{
            fontFamily: theme.regFont,
            color: "#fff",
            fontSize: aspectSizer(18)
          }}
        >
          {year} - {item.winner}
        </Text>
      </LinearGradient>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.renderWinner()}
        <View style={styles.content}>
          {this.renderHead()}
          {this.renderTimeline()}
          {this.renderDetails()}
        </View>
      </View>
    );
  }
}

export default JourneyCard;

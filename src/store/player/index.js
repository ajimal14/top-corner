import reducerRegistry from "../reducerRegistry";

import { SportsAPI, createActionName } from "../utils";

const initialState = {
  error: false,
  loading: false,
  errorData: {},
  data: {},
  selected: null
};

export const reducerName = "getPlayerDetails";

const url = "?action=get_players";

export const getPlayerDetailsData = playerID => {
  return SportsAPI.get(url + `&player_id=${playerID}`)
    .then(resopnse => {
      return resopnse;
    })
    .catch(error => {
      if (error.response) {
        return error.response;
      } else if (error.request) {
        return error.request;
      } else {
        return { status: 400, message: error.message };
      }
    });
};

// SELECTORS //
export const selectData = state => {
  return state[reducerName] ? state[reducerName].data : initialState.data;
};
export const selectActivePlayer = state => {
  return state[reducerName]
    ? state[reducerName].selected
    : initialState.selected;
};
export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;
export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const GET_PLAYER_LOADING = createActionName(reducerName, "LOADING");
export const GET_PLAYER_SUCCESS = createActionName(reducerName, "SUCCESS");
export const GET_PLAYER_ERROR = createActionName(reducerName, "ERROR");
export const SET_ACTIVE_PLAYER = createActionName(reducerName, "ACTIVE");

// ACTION CREATORS //
export const getPlayerDetailsLoading = () => {
  return {
    type: GET_PLAYER_LOADING
  };
};

export const getPlayerDetailsSuccess = payload => {
  return {
    type: GET_PLAYER_SUCCESS,
    payload
  };
};

export const getPlayerDetailsError = error => {
  return {
    type: GET_PLAYER_ERROR,
    error
  };
};

export const setActivePlayer = payload => {
  return {
    type: SET_ACTIVE_PLAYER,
    payload
  };
};

export const getPlayerDetails = playerID => dispatch => {
  dispatch(getPlayerDetailsLoading());
  getPlayerDetailsData(playerID).then(response => {
    if (response.status > 0 && response.status < 400) {
      const data = { [playerID]: response.data };
      dispatch(getPlayerDetailsSuccess(data));
    } else {
      dispatch(getPlayerDetailsError(response));
    }
  });
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case GET_PLAYER_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_PLAYER_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        data: { ...state.data, ...action.payload }
      };
    case GET_PLAYER_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.error
      };
    case SET_ACTIVE_PLAYER:
      return {
        ...state,
        selected: action.payload
      };
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

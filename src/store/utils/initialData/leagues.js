export const topLeagues = {
  [68]: [
    {
      country_id: "68",
      country_name: "Italy",
      league_id: "8726",
      league_name: "Coppa Italia",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/8726_coppa-italia.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/68_italy.png"
    },
    {
      country_id: "68",
      country_name: "Italy",
      league_id: "262",
      league_name: "Serie A",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/262_serie-a.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/68_italy.png"
    },
    {
      country_id: "68",
      country_name: "Italy",
      league_id: "8727",
      league_name: "Super Cup",
      league_season: "2019",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/8727_super-cup.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/68_italy.png"
    }
  ],
  [41]: [
    {
      country_id: "41",
      country_name: "England",
      league_id: "148",
      league_name: "Premier League",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/148_premier-league.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/41_england.png"
    },
    {
      country_id: "41",
      country_name: "England",
      league_id: "8639",
      league_name: "FA Cup",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/8639_fa-cup.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/41_england.png"
    },
    {
      country_id: "41",
      country_name: "England",
      league_id: "8641",
      league_name: "FA Community Shield",
      league_season: "2019",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/8641_fa-community-shield.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/41_england.png"
    },
    {
      country_id: "41",
      country_name: "England",
      league_id: "149",
      league_name: "Championship",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/149_championship.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/41_england.png"
    },
    {
      country_id: "41",
      country_name: "England",
      league_id: "8640",
      league_name: "EFL Cup",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/8640_efl-cup.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/41_england.png"
    },
    {
      country_id: "41",
      country_name: "England",
      league_id: "8401",
      league_name: "EFL Trophy",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/8401_efl-trophy.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/41_england.png"
    }
  ],
  [135]: [
    {
      country_id: "135",
      country_name: "Spain",
      league_id: "468",
      league_name: "LaLiga",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/468_laliga.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/135_spain.png"
    },
    {
      country_id: "135",
      country_name: "Spain",
      league_id: "8760",
      league_name: "Copa del Rey",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/8760_copa-del-rey.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/135_spain.png"
    },
    {
      country_id: "135",
      country_name: "Spain",
      league_id: "8761",
      league_name: "Copa Federacion",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/8761_copa-federacion.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/135_spain.png"
    }
  ],
  [46]: [
    {
      country_id: "46",
      country_name: "France",
      league_id: "176",
      league_name: "Ligue 1",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/176_ligue-1.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/46_france.png"
    },

    {
      country_id: "46",
      country_name: "France",
      league_id: "183",
      league_name: "Coupe de la Ligue",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/183_coupe-de-la-ligue.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/46_france.png"
    },
    {
      country_id: "46",
      country_name: "France",
      league_id: "184",
      league_name: "Coupe de France",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/184_coupe-de-france.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/46_france.png"
    },
    {
      country_id: "46",
      country_name: "France",
      league_id: "185",
      league_name: "Super Cup",
      league_season: "2019",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/185_super-cup.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/46_france.png"
    }
  ],
  [51]: [
    {
      country_id: "51",
      country_name: "Germany",
      league_id: "195",
      league_name: "Bundesliga",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/195_bundesliga.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/51_germany.png"
    },
    {
      country_id: "51",
      country_name: "Germany",
      league_id: "8700",
      league_name: "DFB Pokal",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/8700_dfb-pokal.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/51_germany.png"
    },
    {
      country_id: "51",
      country_name: "Germany",
      league_id: "8701",
      league_name: "Super Cup",
      league_season: "2019",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/8701_super-cup.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/51_germany.png"
    }
  ],

  [100]: [
    {
      country_id: "100",
      country_name: "Netherlands",
      league_id: "343",
      league_name: "Eredivisie",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/343_eredivisie.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/100_netherlands.png"
    },
    {
      country_id: "100",
      country_name: "Netherlands",
      league_id: "348",
      league_name: "Super Cup",
      league_season: "2019",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/348_super-cup.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/100_netherlands.png"
    }
  ]
};

import React from "react";
import {
  TouchableNativeFeedback,
  View,
  Image,
  Text,
  StyleSheet
} from "react-native";
import theme from "tlc/theme";
import { aspectSizer } from "utils";

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
    backgroundColor: "#fff",
    margin: 5,
    justifyContent: "center",
    alignItems: "center"
  },
  team: {
    fontSize: aspectSizer(15),
    fontFamily: theme.boldFont,
    textTransform: "uppercase",
    marginVertical: 10,
    color: theme.primaryTextColor,
    textAlign:'center'
  },
  image: {
    width: aspectSizer(70),
    height: aspectSizer(70),
    alignSelf: "center"
  }
});

const TeamCard = ({ item, onPress }) => (
  <TouchableNativeFeedback
    background={TouchableNativeFeedback.Ripple("#e3e3e3")}
    onPress={() => onPress(item)}
  >
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={{ uri: item.team_badge }}
        resizeMode="contain"
      />
      <Text style={styles.team}>{item.team_name}</Text>
    </View>
  </TouchableNativeFeedback>
);

export default TeamCard;

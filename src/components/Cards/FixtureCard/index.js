import React from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import theme from "tlc/theme";

const styles = StyleSheet.create({
  container: {
    margin: 10
  },
  content: {
    backgroundColor: "#fff",
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderRadius: 10,
    shadowColor: "#e3e3e3",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.95,
    shadowRadius: 3.84,

    elevation: 5
  },
  logo: {
    width: 60,
    height: 60,
    borderRadius: 10,
    overflow: "hidden",
    marginBottom: 20
  },
  matchdate: {
    marginLeft: 10,
    color: "grey",
    fontSize: 10,
    fontFamily: theme.regFont,
    textTransform: "uppercase"
  },
  teamName: {
    fontFamily: theme.boldFont,
    color: "darkgrey",
    textTransform: "uppercase",
    textAlign: "center"
  }
});

class FixtureCard extends React.Component {
  renderTimeline = () => {
    const {
      item: { goalscorer, match_id }
    } = this.props;
    return goalscorer.map((goal, index) => {
      return (
        <Text
          key={"key" + index + match_id}
          style={{
            textAlign: goal.away_scorer === "" ? "left" : "right",
            marginVertical: 5
          }}
        >
          <Ionicons name="ios-football" color="grey" />{" "}
          <Text
            style={{
              color: "grey",
              fontFamily: theme.regFont
            }}
          >
            {goal.home_scorer !== "" ? goal.home_scorer : goal.away_scorer} at{" "}
            {goal.time}m
          </Text>
        </Text>
      );
    });
  };

  renderScore = () => {
    const {
      item: { goalscorer, match_status }
    } = this.props;

    const scoresConfirmed =
      match_status === "Finished" ||
      match_status === "After ET" ||
      match_status === "After Pen.";

    const statusText = match_status === "" ? "TBD" : match_status;

    const score =
      goalscorer.length > 0 && goalscorer[goalscorer.length - 1].score
        ? goalscorer[goalscorer.length - 1].score
        : "0 - 0";

    const finalScore = scoresConfirmed ? { score } : { score: statusText };

    return (
      <View
        style={{
          width: "40%",
          alignItems: "center"
        }}
      >
        <Text
          style={{
            fontFamily: theme.boldFont,
            fontSize: scoresConfirmed ? 40 : 12.5,
            color: scoresConfirmed ? "grey" : theme.secondaryColor,
            borderBottomColor: scoresConfirmed
              ? "#fff"
              : theme.secondaryColorLight,
            borderBottomWidth: scoresConfirmed ? 0 : 2
          }}
        >
          {finalScore.score}
        </Text>
      </View>
    );
  };

  renderSide = side => {
    const { item } = this.props;
    const redCards = item.cards.filter(
      booking => booking.card === "red card" && booking[`${side}_fault`] !== ""
    );

    return (
      <View
        style={{
          width: "30%",
          alignItems: "center"
        }}
      >
        <Image
          style={styles.logo}
          source={{ uri: item[`team_${side}_badge`] }}
        />
        <Text style={styles.teamName}>{item[`match_${side}team_name`]}</Text>
        <View style={{ flexDirection: "row", justifyContent: "flex-start" }}>
          {redCards.map(card => (
            <View style={{ marginLeft: 2 }}>
              <Ionicons name="ios-square" color="red" />
            </View>
          ))}
        </View>
      </View>
    );
  };

  render() {
    const {
      item: { country_name, match_date, match_time }
    } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <Text
            style={{
              fontFamily: theme.regFont,
              textTransform: "uppercase",
              color: theme.secondaryColorLight
            }}
          >
            {country_name}
          </Text>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-around",
              alignItems: "center",
              marginVertical: 20
            }}
          >
            {this.renderSide("home")}
            {this.renderScore()}
            {this.renderSide("away")}
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center"
            }}
          >
            <Ionicons name="md-calendar" size={15} color="grey" />
            <Text style={styles.matchdate}>
              {match_time},{new Date(match_date).toDateString()}
            </Text>
          </View>
        </View>
        <View style={{ padding: 10 }}>{this.renderTimeline()}</View>
      </View>
    );
  }
}

export default FixtureCard;

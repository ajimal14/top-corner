import reducerRegistry from "../reducerRegistry";

import { SportsAPI, createActionName } from "../utils";

const initialState = {
  error: false,
  loading: false,
  errorData: {},
  data: {},
  selected: null,
  top: [
    {
      country_id: "41",
      country_name: "England",
      badge:
        "https://upload.wikimedia.org/wikipedia/en/thumb/8/8b/England_national_football_team_crest.svg/1200px-England_national_football_team_crest.svg.png"
    },
    {
      country_id: "68",
      country_name: "Italy",
      badge:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/3/33/FIGC_Logo_2017.svg/1200px-FIGC_Logo_2017.svg.png"
    },
    {
      country_id: "135",
      country_name: "Spain",
      badge:
        "https://upload.wikimedia.org/wikipedia/en/thumb/3/39/Spain_national_football_team_crest.svg/1200px-Spain_national_football_team_crest.svg.png"
    },
    {
      country_id: "51",
      country_name: "Germany",
      badge:
        "https://upload.wikimedia.org/wikipedia/en/thumb/e/e3/DFBEagle.svg/1200px-DFBEagle.svg.png"
    },
    {
      country_id: "46",
      country_name: "France",
      badge:
        "https://upload.wikimedia.org/wikipedia/en/thumb/2/23/French_Football_Federation_logo.svg/1200px-French_Football_Federation_logo.svg.png"
    },
    {
      country_id: "100",
      country_name: "Netherlands",
      badge:
        "https://upload.wikimedia.org/wikipedia/en/thumb/7/78/Netherlands_national_football_team_logo.svg/1200px-Netherlands_national_football_team_logo.svg.png"
    }
  ]
};

export const reducerName = "getCountryList";

const url = "?action=get_countries";

export const getCountryListData = () => {
  return SportsAPI.get(url)
    .then(resopnse => {
      return resopnse;
    })
    .catch(error => {
      if (error.response) {
        return error.response;
      } else if (error.request) {
        return error.request;
      } else {
        return { status: 400, message: error.message };
      }
    });
};

// SELECTORS //
export const selectData = state => {
  return state[reducerName] ? state[reducerName].data : initialState.data;
};
export const selectTopCountry = state => {
  return state[reducerName] ? state[reducerName].top : initialState.top;
};
export const selectActiveCountry = state => {
  return state[reducerName]
    ? state[reducerName].selected
    : initialState.selected;
};
export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;
export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const GET_COUNTRY_LOADING = createActionName(reducerName, "LOADING");
export const GET_COUNTRY_SUCCESS = createActionName(reducerName, "SUCCESS");
export const GET_COUNTRY_ERROR = createActionName(reducerName, "ERROR");
export const SET_ACTIVE_COUNTRY = createActionName(reducerName, "ACTIVE");

// ACTION CREATORS //
export const getCountryListLoading = () => {
  return {
    type: GET_COUNTRY_LOADING
  };
};

export const getCountryListSuccess = payload => {
  return {
    type: GET_COUNTRY_SUCCESS,
    payload
  };
};

export const getCountryListError = error => {
  return {
    type: GET_COUNTRY_ERROR,
    error
  };
};

export const setActiveCountry = payload => {
  return {
    type: SET_ACTIVE_COUNTRY,
    payload
  };
};

export const getCountryList = () => dispatch => {
  dispatch(getCountryListLoading());
  getCountryListData().then(response => {
    if (response.status > 0 && response.status < 400) {
      const data = response.data;
      dispatch(getCountryListSuccess(data));
    } else {
      dispatch(getCountryListError(response));
    }
  });
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case GET_COUNTRY_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_COUNTRY_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        data: action.payload
      };
    case GET_COUNTRY_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.error
      };
    case SET_ACTIVE_COUNTRY:
      return {
        ...state,
        selected: action.payload
      };
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

import React from "react";
import { TouchableNativeFeedback, Text, View, StyleSheet } from "react-native";
import theme from "tlc/theme";
import { aspectSizer } from "utils";

const styles = StyleSheet.create({
  container: {
    paddingVertical: 15,
    paddingHorizontal: 20,
    backgroundColor: theme.primaryColor,
    alignSelf: "center",
    marginVertical: 10,
    borderRadius: 50
  },
  text: {
    fontFamily: theme.boldFont,
    textTransform: "uppercase",
    color: "#FFF",
    fontSize: aspectSizer(15)
  }
});

const PrimaryButton = props => (
  <TouchableNativeFeedback
    background={TouchableNativeFeedback.Ripple("#e3e3e3")}
    onPress={props.onPress}
  >
    <View style={styles.container}>
      <Text style={styles.text}>{props.children}</Text>
    </View>
  </TouchableNativeFeedback>
);

export default PrimaryButton;

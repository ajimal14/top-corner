import React from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import Navigation from "tlc/navigation";
import Modals from "tlc/modals";
import * as Font from "expo-font";
import Loader from "components/Loader";

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  loader: {
    justifyContent: "center",
    alignSelf: "center"
  }
});

class Main extends React.Component {
  state = {
    fontLoaded: false
  };
  async componentDidMount() {
    await Font.loadAsync({
      raleway: require("../assets/fonts/Raleway-Regular.ttf"),
      "raleway-bold": require("../assets/fonts/Raleway-ExtraBold.ttf")
    });

    this.setState({ fontLoaded: true });
  }

  render() {
    const { fontLoaded } = this.state;
    
    if (fontLoaded) {
      return (
        <SafeAreaView style={styles.container}>
          <Navigation />
          <Modals />
        </SafeAreaView>
      );
    } else
      return (
        <SafeAreaView style={[styles.container, styles.loader]}>
          <Loader />
        </SafeAreaView>
      );
  }
}

export default Main;

import React from "react";
import { StyleSheet, View, FlatList, SafeAreaView } from "react-native";
import { connect } from "react-redux";
import styled from "styled-components/native";

import {
  getCountryList,
  selectErrorStatus,
  selectLoadingStatus,
  selectData,
  selectTopCountry,
  setActiveCountry,
  selectActiveCountry
} from "store/country";
import Loader from "components/Loader";
import FadedHeading from "components/FadedHeading";
import CountryCard from "components/Cards/CountryCard";
import PrimaryButton from "../../components/Buttons/PrimaryButton";

const TopCountryWrapper = styled.View`
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`;

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

class CountryPicker extends React.Component {
  fetchCountryList = () => {
    const { getCountryList } = this.props;
    getCountryList();
  };

  handleClick = ({ country_id }) => {
    const { navigation, setActiveCountry } = this.props;
    setActiveCountry(country_id);
    navigation.navigate("League", {
      countryID: country_id
    });
  };

  renderLoadMoreCountry = () => {
    const { loading } = this.props;

    if (loading) {
      return <Loader />;
    } else {
      return (
        <PrimaryButton onPress={this.fetchCountryList}>
          Select from More Countries
        </PrimaryButton>
      );
    }
  };

  render() {
    const { top } = this.props;
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <FadedHeading>Select Country</FadedHeading>
          <TopCountryWrapper>
            <FlatList
              data={top}
              renderItem={item => (
                <CountryCard
                  {...item}
                  onPress={country => this.handleClick(country)}
                />
              )}
              keyExtractor={item => item.country_id}
              numColumns={3}
            />
          </TopCountryWrapper>
          {/* {this.renderLoadMoreCountry()} */}
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  loading: selectLoadingStatus(state),
  data: selectData(state),
  top: selectTopCountry(state),
  error: selectErrorStatus(state),
  active: selectActiveCountry(state)
});

const mapDispatchToProps = dispatch => ({
  getCountryList: () => dispatch(getCountryList()),
  setActiveCountry: id => dispatch(setActiveCountry(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CountryPicker);

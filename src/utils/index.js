import { Dimensions, Platform, PixelRatio } from "react-native";

export const debounce = (fn, time) => {
  let timeout;

  return function() {
    const functionCall = () => fn.apply(this, arguments);

    clearTimeout(timeout);
    timeout = setTimeout(functionCall, time);
  };
};

export const positionEnum = {
  Goalkeepers: "GK",
  Defenders: "DEF",
  Midfielders: "MID",
  Forwards: "FWD"
};

export const aspectSizer = size => {
  const { width } = Dimensions.get("window");
  if (width > 400) {
    return size;
  } else if (width < 400 && width > 370) {
    return size * 0.8;
  } else {
    return size * 0.7;
  }
};

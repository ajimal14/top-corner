import React from "react";
import {
  StyleSheet,
  ScrollView,
  Text,
  View,
  SafeAreaView,
  ImageBackground
} from "react-native";

import CL from "utils/raw-data/champions-league";
import JourneyCard from "components/Cards/JourneyCard";

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

const ChampionsLeague = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground
        source={{
          uri: "https://i.imgur.com/JeN7KFh.png"
        }}
        style={{ width: "100%", height: "100%", backgroundColor: "#0b096a" }}
        resizeMode="cover"
        blurRadius={2}
      >
        <ScrollView>
          {Object.keys(CL)
            .sort(a => -1)
            .map(key => (
              <JourneyCard key={key} item={CL[key]} year={key} />
            ))}
        </ScrollView>
      </ImageBackground>
    </SafeAreaView>
  );
};

export default ChampionsLeague;

import React from "react";
import { TouchableOpacity, View, Image, Text, StyleSheet } from "react-native";
import theme from "tlc/theme";
import { aspectSizer } from "utils";

const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
    width: '30%',
    alignSelf: "auto",
    marginLeft: "auto",
    marginRight: "auto",
    marginBottom: 15
  },
  team: {
    fontSize: aspectSizer(10),
    textAlign: 'center',
    fontFamily: theme.boldFont,
    textTransform: "uppercase",
    marginVertical: 10,
    color: theme.primaryTextColor
  },
  image: {
    width: aspectSizer(70),
    height: aspectSizer(70),
    alignSelf: "center"
  }
});

const LeagueCard = ({ item, onPress }) => (
  <TouchableOpacity onPress={() => onPress(item)} style={styles.container}>
    <Image
      style={styles.image}
      source={{ uri: item.league_logo }}
      resizeMode="contain"
    />
    <Text style={styles.team}>{item.league_name}</Text>
  </TouchableOpacity>
);

export default LeagueCard;

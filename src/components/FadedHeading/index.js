import React from "react";
import { Text } from "react-native";
import { aspectSizer } from "../../utils";
import theme from "tlc/theme";

const FadedHeading = props => (
  <Text
    style={{
      padding: 20,
      fontSize: aspectSizer(30),
      textAlign: "center",
      fontFamily: theme.boldFont,
      textTransform: "uppercase",
      color: "#e3e3e3"
    }}
  >
    {props.children}
  </Text>
);

export default FadedHeading;

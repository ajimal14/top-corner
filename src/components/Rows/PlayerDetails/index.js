import React from "react";
import { TouchableNativeFeedback, View, Text, StyleSheet } from "react-native";
import { positionEnum,aspectSizer } from "utils";
import theme from "tlc/theme";

const styles = StyleSheet.create({
  item: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingVertical: 10,
    flexWrap: "wrap",
    borderBottomColor: theme.borderColor,
    borderBottomWidth: 1
  },
  playerName: {
    fontFamily: theme.boldFont,
    color: theme.secondaryColor,
    fontSize: aspectSizer(15)
  },
  text: {
    fontFamily: theme.regFont,
    color: "grey",
    fontSize: aspectSizer(15)
  }
});

const PlayerDetailsRow = ({
  item: {
    player_number,
    player_name,
    player_type,
    player_key,
    player_goals,
    player_country,
    player_age
  },
  listStyles,
  onPress = {}
}) => {
  return (
    <TouchableNativeFeedback
      background={TouchableNativeFeedback.Ripple(theme.primaryColor)}
      onPress={() => onPress(player_key)}
    >
      <View key={player_number} style={styles.item}>
        <View style={listStyles.PlayerDetails}>
          <Text style={styles.playerName}>{player_name}</Text>
          <Text style={styles.text}>
            Kit - {player_number} ({positionEnum[player_type]})
          </Text>
        </View>

        <Text style={[listStyles.Country, styles.text]}>{player_country}</Text>

        <Text style={[listStyles.Goals, styles.text]}>{player_goals}</Text>
        <Text style={[listStyles.Age, styles.text]}>{player_age} Yrs</Text>
      </View>
    </TouchableNativeFeedback>
  );
};

export default PlayerDetailsRow;

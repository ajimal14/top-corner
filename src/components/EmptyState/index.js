import React from "react";
import { View, Text } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import theme from "tlc/theme";

const EmptyState = ({ text, icon }) => (
  <View
    style={{
      flex: 1,
      alignItems: "center",
      justifyContent: "center"
    }}
  >
    <Ionicons name={icon || "ios-search"} size={140} color="#e3e3e3" />
    <Text
      style={{
        fontFamily: theme.boldFont,
        fontSize: 22,
        color: "grey",
        textTransform: "uppercase"
      }}
    >
      {text || " No Result Found"}
    </Text>
  </View>
);

export default EmptyState;

![React JS](https://img.shields.io/badge/React%20Native-React%20--Redux-lightgrey)
# Welcome to Top-Corner!

Its a Football First Application for Your Football Needs.
From a Football Fan.

![Paris Saint Germain](https://img.shields.io/badge/Paris%20Saint%20Germain-ici%20c'est%20paris-blue?style=for-the-badge)

# Files

Structure of Application
## Components

These are the Dumb Components Mostly
## Screens

These are the Components that are Rendered by The Navigation Stack.
## TLC

These are Top Level Components (TLC) are those components that rendered by the App Itself.

 - [ ] Routes
 - [ ] Modals
 - [ ] Theme 

## Store

We are using Redux for State Management and We are using Redux Ducks (FS Architecture ) for Code Splitting and Management.
## Utils

Reusable Functions
# React Native - Expo

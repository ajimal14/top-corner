import reducerRegistry from "../reducerRegistry";

import { createActionName } from "../utils";

const initialState = {
  type: null,
  props: {}
};

const reducerName = "modal";

// SELECTORS //
export const selectModalType = state => state[reducerName].type;
export const selectModalProps = state => state[reducerName].props;

// ACTIONS //
export const SHOW_MODAL = createActionName(reducerName, "SHOW_MODAL");
export const HIDE_MODAL = createActionName(reducerName, "HIDE_MODAL");

// ACTION CREATORS //
export const showModal = (type, props) => ({
  type: SHOW_MODAL,
  payload: {
    type,
    props
  }
});

export const hideModal = (type, props) => ({
  type: HIDE_MODAL,
  payload: {
    type,
    props
  }
});

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SHOW_MODAL:
      return {
        ...state,
        type: action.payload.type,
        props: { show: true, ...action.payload.props }
      };
    case HIDE_MODAL:
      return {
        ...state,
        type: action.payload.type,
        props: { show: false, ...action.payload.props }
      };
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

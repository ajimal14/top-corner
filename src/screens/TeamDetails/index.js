import React from "react";
import { StyleSheet, Text, View, SafeAreaView } from "react-native";
import { connect } from "react-redux";

import { showModal } from "store/modal";
import { selectActiveTeam } from "store/teams";

import SortableList from "components/SortableList";
import theme from "tlc/theme";
import TeamBadge from "components/TeamBadge";
import PlayerDetailsRow from "components/Rows/PlayerDetails";
import { aspectSizer } from "utils";

const listStyles = {
  PlayerDetails: {
    width: "30%"
  },
  Country: {
    width: "30%"
  },
  Goals: {
    width: "15%"
  },
  Age: {
    width: "15%"
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginHorizontal: 10
  },
  coach: {
    padding: 5,
    width: "45%"
  },
  coachName: {
    fontFamily: "raleway-bold",
    fontSize: aspectSizer(15),
    color: theme.primaryColor,
    textTransform: "uppercase"
  },
  coachCountry: {
    color: theme.primaryColorLight,
    fontFamily: theme.boldFont,
    fontSize: aspectSizer(12)
  },
  ...listStyles
});

class TeamDetails extends React.Component {
  handleClick = playerID => {
    const { showModal } = this.props;
    showModal("PLAYER_DETAILS", { playerID });
  };

  headers = [
    {
      name: "Name",
      key: "player_name",
      type: "string",
      style: styles.PlayerDetails
    },
    {
      name: "Nationality",
      key: "player_country",
      type: "string",
      style: styles.Country
    },
    {
      name: "Goals",
      key: "player_goals",
      type: "numeric",
      style: styles.Goals
    },
    { name: "Age", key: "player_age", type: "numeric", style: styles.Age }
  ];

  filters = [
    {
      name: "< 2 Match",
      filter: player => player.player_match_played < 2
    },
    {
      name: "> 2 Match",
      filter: player => player.player_match_played >= 2
    }
  ];

  renderContent = () => {
    const { team } = this.props;
    const coach = team && team.coaches[0];
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center"
        }}
      >
        <TeamBadge team={team} />
        <View style={styles.coach}>
          <Text style={[{ color: "#80cbc4", fontSize: 12 }]}>Head Coach</Text>
          <Text style={styles.coachName}>
            {coach &&
              coach.coach_name
                .split(" ")
                .reverse()
                .join(" ")}
          </Text>
          <Text style={styles.coachCountry}>
            {coach && coach.coach_country}
          </Text>
        </View>
      </View>
    );
  };

  render() {
    const { team } = this.props;
    const { players } = team;
    return (
      <SafeAreaView style={styles.container}>
        {this.renderContent()}
        <SortableList
          list={players}
          component={item => (
            <PlayerDetailsRow
              listStyles={styles}
              {...item}
              onPress={this.handleClick}
            />
          )}
          headers={this.headers}
          filters={this.filters}
        />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  team: selectActiveTeam(state)
});

const mapDispatchToProps = dispatch => ({
  showModal: (type, props) => dispatch(showModal(type, props))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TeamDetails);

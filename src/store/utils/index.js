import axios from "axios";
import { appName, host, APIkey } from "./constants";

export const createActionName = (reducerName, actionName) => {
  return `${appName}/${reducerName}/${actionName}`;
};

export const SportsAPI = axios.create({
  baseURL: host,
  params: {
    APIkey
  }
});

SportsAPI.interceptors.request.use(function(config) {
  let prefix = "?";
  if (config.url.includes("?")) {
    prefix = "&";
  }
  config.url = config.url + prefix + "APIkey=" + APIkey;
  return config;
});

export const formatDate = month => {
  let date = new Date();
  const from = `${date.getFullYear()}-${month}-1`;
  const to = `${date.getFullYear()}-${month}-${new Date(
    date.getFullYear(),
    month,
    0
  ).getDate()}`;
  return {
    from,
    to
  }
};

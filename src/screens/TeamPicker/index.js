import React from "react";
import { StyleSheet, Text, SafeAreaView, FlatList } from "react-native";
import { connect } from "react-redux";

import {
  getTeamList,
  selectErrorStatus,
  selectLoadingStatus,
  selectData,
  setActiveTeam,
  selectActiveTeam
} from "store/teams";

import Loader from "components/Loader";
import FadedHeading from "components/FadedHeading";
import TeamCard from "components/Cards/TeamCard";
import EmptyState from "../../components/EmptyState";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 5
  },
  list: {}
});

class TeamPicker extends React.Component {
  componentDidMount() {
    const {
      getTeamList,
      route: {
        params: { leagueID }
      },
      data
    } = this.props;
    if (!data.hasOwnProperty(leagueID)) {
      getTeamList(leagueID);
    }
  }

  handleClick = team => {
    const { setActiveTeam, navigation } = this.props;
    setActiveTeam(team);
    navigation.navigate("Team Details");
  };

  renderContent = () => {
    const {
      loading,
      data,
      route: {
        params: { leagueID }
      }
    } = this.props;

    const teams = data[leagueID] || [];

    if (loading) {
      return <Loader />;
    } else if (teams.length > 0) {
      return (
        <FlatList
          data={teams}
          renderItem={item => <TeamCard onPress={this.handleClick} {...item} />}
          keyExtractor={item => item.team_name}
          contentContainerStyle={styles.list}
          numColumns={2}
        />
      );
    } else {
      return <EmptyState />;
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <FadedHeading>Pick Your Team</FadedHeading>
        {this.renderContent()}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  loading: selectLoadingStatus(state),
  data: selectData(state),
  error: selectErrorStatus(state),
  active: selectActiveTeam(state)
});

const mapDispatchToProps = dispatch => ({
  getTeamList: leagueID => dispatch(getTeamList(leagueID)),
  setActiveTeam: data => dispatch(setActiveTeam(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TeamPicker);

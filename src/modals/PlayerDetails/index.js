import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Modal,
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
  Animated
} from "react-native";

import {
  getPlayerDetails,
  selectLoadingStatus,
  selectData,
  selectErrorStatus
} from "store/player";
import { hideModal, selectModalType, selectModalProps } from "store/modal";

import Loader from "components/Loader";
import theme from "../../top-level-components/theme";
import { aspectSizer } from "../../utils";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end"
  },
  pill: {
    width: 40,
    height: 12.5,
    backgroundColor: "#ffffff80",
    borderRadius: 50,
    alignSelf: "center",
    marginVertical: 10
  },
  playerDetails: {
    backgroundColor: "#fff",
    padding: 20,
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
    position: "absolute",
    left: 0,
    width: "100%"
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "80%",
    alignSelf: "center",
    borderBottomColor: theme.borderColor,
    borderBottomWidth: 1,
    paddingVertical: 10,
    flexWrap: "wrap"
  },
  key: {
    flex: 1,
    fontSize: aspectSizer(15),
    textAlign: "left",
    fontFamily: theme.regFont
  },
  value: {
    flex: 1,
    fontFamily: theme.boldFont,
    fontSize: aspectSizer(15),
    textAlign: "right"
  }
});

class PlayerDetails extends Component {
  state = {
    bottom: new Animated.Value(-500),
    opacity: new Animated.Value(0)
  };

  componentDidMount() {
    const { getPlayerDetails, playerID, data } = this.props;

    this.handleOpen();
    if (!data[playerID]) {
      getPlayerDetails(playerID);
    } else {
      this.slideUp();
    }
  }

  componentDidUpdate(prevProps) {
    const { data, playerID } = this.props;
    if (prevProps.data[playerID] !== data[playerID]) {
      this.slideUp();
    }
  }
  
  slideUp = () => {
    Animated.timing(this.state.bottom, {
      toValue: 0,
      duration: 500
    }).start();
  };

  handleOpen = () => {
    Animated.timing(this.state.opacity, {
      toValue: 1,
      duration: 500
    }).start();
  };

  handleClose = () => {
    Animated.timing(this.state.bottom, {
      toValue: -500,
      duration: 500
    }).start(() => {
      this.props.hideModal();
    });
    Animated.timing(this.state.opacity, {
      toValue: 0,
      duration: 500
    }).start();
  };

  render() {
    const { show, loading, data, playerID } = this.props;
    const player = data[playerID] ? data[playerID][0] : null;

    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={show}
        onRequestClose={() => {
          this.handleClose();
        }}
      >
        <Animated.View
          style={[styles.container, { opacity: this.state.opacity }]}
        >
          <TouchableOpacity
            style={{
              width: "100%",
              height: "100%",
              alignSelf: "center",
              position: "absolute",
              top: 0,
              left: 0,
              backgroundColor: "rgba(0,0,0,0.8)"
            }}
            activeOpacity={1}
            onPress={() => this.handleClose()}
          />

          {loading && <Loader />}
          {player && (
            <Animated.View
              style={[styles.playerDetails, { bottom: this.state.bottom }]}
            >
              <View style={styles.row}>
                <Text style={styles.key}>Player Name</Text>
                <Text style={styles.value}>{player.player_name}</Text>
              </View>
              <View style={styles.row}>
                <Text style={styles.key}>Kit Number</Text>
                <Text style={styles.value}>{player.player_number}</Text>
              </View>
              <View style={styles.row}>
                <Text style={styles.key}>Country</Text>
                <Text style={styles.value}>{player.player_country}</Text>
              </View>
              <View style={styles.row}>
                <Text style={styles.key}>Postion</Text>
                <Text style={styles.value}>{player.player_type}</Text>
              </View>

              <View style={styles.row}>
                <Text style={styles.key}>Current Club</Text>
                <Text style={styles.value}>{player.team_name}</Text>
              </View>

              <View style={styles.row}>
                <Text style={styles.key}>Total Match Played</Text>
                <Text style={styles.value}>{player.player_match_played}</Text>
              </View>

              <View style={[styles.row, { borderBottomWidth: 0 }]}>
                <Text style={styles.key}>Goals</Text>
                <Text style={styles.value}>{player.player_goals}</Text>
              </View>
            </Animated.View>
          )}
        </Animated.View>
      </Modal>
    );
  }
}

const mapStateToProps = state => ({
  loading: selectLoadingStatus(state),
  data: selectData(state),
  error: selectErrorStatus(state),
  type: selectModalType(state),
  modalProps: selectModalProps(state)
});

const mapDispatchToProps = dispatch => ({
  getPlayerDetails: id => dispatch(getPlayerDetails(id)),
  hideModal: (type, props) => dispatch(hideModal(type, props))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlayerDetails);

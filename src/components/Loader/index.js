import React from "react";
import { ActivityIndicator } from "react-native";
import theme from "tlc/theme";

export default function Loader() {
  return <ActivityIndicator size="large" color={theme.primaryColor} />;
}

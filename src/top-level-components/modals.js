// Redux Controlled Modal
// Dispatch showModal with Modal Name from Below.

import React from "react";
import { connect } from "react-redux";

import PlayerDetails from "../modals/PlayerDetails";

const MODAL_COMPONENTS = {
  PLAYER_DETAILS: PlayerDetails
};

const ModalRoot = ({ type, props }) => {
  if (!type) {
    return null;
  }

  const ModalComponent = MODAL_COMPONENTS[type];
  return <ModalComponent {...props} />;
};

const mapStateToProps = state => ({
  type: state.modal.type,
  props: state.modal.props
});

export default connect(mapStateToProps)(ModalRoot);

import * as React from "react";
import Country from "screens/CountryPicker";
import Leagues from "screens/LeaguePicker";
import TeamDetails from "screens/TeamDetails";
import Team from "screens/TeamPicker";
import { createStackNavigator } from "@react-navigation/stack";

import theme from "tlc/theme";

const Home = createStackNavigator();

const HomeStack = () => (
  <Home.Navigator
    initialRouteName="Country"
    screenOptions={{
      headerStyle: {
        backgroundColor: theme.primaryColor
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontFamily: theme.boldFont,
        color: theme.primaryColorLight
      }
    }}
  >
    <Home.Screen name="Home" component={Country} />
    <Home.Screen name="League" component={Leagues} />
    <Home.Screen name="Team" component={Team} />
    <Home.Screen name="Team Details" component={TeamDetails} />
  </Home.Navigator>
);

export default HomeStack;

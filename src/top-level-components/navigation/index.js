import * as React from "react";
import { SafeAreaView } from "react-native";
import { Ionicons } from "@expo/vector-icons";

import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import HomeStack from "./stacks/home";
import EventStack from "./stacks/event";
import theme from "../theme";

const Tab = createMaterialBottomTabNavigator();

const Tabs = () => (
  <SafeAreaView
    style={{
      flex: 1
    }}
  >
    <NavigationContainer>
      <Tab.Navigator
        shifting={true}
        keyboardHidesTabBar={true}
        initialRouteName="Home"
      >
        <Tab.Screen
          name="Home"
          component={HomeStack}
          tabBarColor={theme.primaryColor}
          options={{
            tabBarColor: theme.primaryColor,
            tabBarIcon: ({ color }) => (
              <Ionicons name="ios-home" size={25} color={color} />
            )
          }}
        />
        <Tab.Screen
          name="Event"
          component={EventStack}
          options={{
            tabBarColor: theme.secondaryColor,
            tabBarIcon: ({ color }) => (
              <Ionicons name="ios-football" size={25} color={color} />
            )
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  </SafeAreaView>
);

export default function Navigation() {
  return <Tabs />;
}

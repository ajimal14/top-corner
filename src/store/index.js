import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import thunk from "redux-thunk";
import reducerRegistry from "./reducerRegistry";
import modal from './modal';
import leagues from './leagues';
const middlewares = [thunk];

const initialState = {
  modal,
  'getLeagueList': leagues
};

// Preserve initial state for not-yet-loaded reducers
const combine = reducers => {
  const reducerNames = Object.keys(reducers);
  Object.keys(initialState).forEach(item => {
    if (reducerNames.indexOf(item) === -1) {
      reducers[item] = (state = null) => state;
    }
  });
  return combineReducers(reducers);
};

const reducer = combine(reducerRegistry.getReducers());
const store = createStore(reducer, initialState, applyMiddleware(...middlewares));

// Replace the store's reducer whenever a new reducer is registered.
reducerRegistry.setChangeListener(reducers => {
  store.replaceReducer(combine(reducers));
});

export default store;

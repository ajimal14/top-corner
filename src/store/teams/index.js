import reducerRegistry from "../reducerRegistry";

import { SportsAPI, createActionName } from "../utils";

const initialState = {
  error: false,
  loading: false,
  errorData: {},
  data: {},
  selected: null
};

export const reducerName = "getTeamList";

const url = "?action=get_teams";

export const getTeamListData = leagueID => {
  return SportsAPI.get(url + `&league_id=${leagueID}`)
    .then(resopnse => {
      return resopnse;
    })
    .catch(error => {
      if (error.response) {
        return error.response;
      } else if (error.request) {
        return error.request;
      } else {
        return { status: 400, message: error.message };
      }
    });
};

// SELECTORS //
export const selectData = state => {
  return state[reducerName] ? state[reducerName].data : initialState.data;
};
export const selectActiveTeam = state => {
  return state[reducerName]
    ? state[reducerName].selected
    : initialState.selected;
};
export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;
export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const GET_TEAMS_LOADING = createActionName(reducerName, "LOADING");
export const GET_TEAMS_SUCCESS = createActionName(reducerName, "SUCCESS");
export const GET_TEAMS_ERROR = createActionName(reducerName, "ERROR");
export const SET_ACTIVE_TEAM = createActionName(reducerName, "ACTIVE");

// ACTION CREATORS //
export const getTeamListLoading = () => {
  return {
    type: GET_TEAMS_LOADING
  };
};

export const getTeamListSuccess = payload => {
  return {
    type: GET_TEAMS_SUCCESS,
    payload
  };
};

export const getTeamListError = error => {
  return {
    type: GET_TEAMS_ERROR,
    error
  };
};

export const setActiveTeam = payload => {
  return {
    type: SET_ACTIVE_TEAM,
    payload
  };
};

export const getTeamList = leagueID => dispatch => {
  dispatch(getTeamListLoading());
  getTeamListData(leagueID).then(response => {
    if (response.status > 0 && response.status < 400) {
      const data = { [leagueID]: response.data };
      dispatch(getTeamListSuccess(data));
    } else {
      dispatch(getTeamListError(response));
    }
  });
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case GET_TEAMS_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_TEAMS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        data: { ...state.data, ...action.payload }
      };
    case GET_TEAMS_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.error
      };
    case SET_ACTIVE_TEAM:
      return {
        ...state,
        selected: action.payload
      };
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

import reducerRegistry from "../reducerRegistry";

import { createActionName } from "../utils";

const initialState = {
  data: [],
  filtered: [],
  search: "",
  key: null,
  direction: "",
  type: null,
  searchableKeys: ["player_name", "player_country"]
};

export const reducerName = "sortable-filter";

function sortByProperty(direction, property, type) {
  return function(a, b) {
    if (type === "string") {
      if (direction === "desc") {
        return a[property].toLowerCase() < b[property].toLowerCase();
      } else {
        return a[property].toLowerCase() > b[property].toLowerCase();
      }
    } else {
      if (direction === "desc") {
        return b[property] - a[property];
      } else {
        return a[property] - b[property];
      }
    }
  };
}

const getSortedList = ({ filtered, type, key, direction }) => {
  let returnValue = filtered;
  const firstRow = filtered[0];
  if (firstRow.hasOwnProperty(key)) {
    returnValue = filtered.sort(sortByProperty(direction, key, type));
  } else {
    returnValue = filtered;
  }
  return returnValue;
};

const getSearchedList = ({ sortedList, search, searchableKeys }) => {
  let returnValue = sortedList;
  if (search && search !== "") {
    returnValue = sortedList.filter(listItem => {
      let returnVal = false;
      searchableKeys.forEach(element => {
        if (listItem[element].includes(search)) {
          returnVal = true;
        }
      });
      return returnVal;
    });
  }
  return returnValue;
};

// SELECTORS //
export const selectData = state => {
  if (state[reducerName]) {
    const { filtered, search, direction, type, key, searchableKeys } = state[
      reducerName
    ];
    let returnValue = filtered;
    if (Array.isArray(filtered) && filtered.length > 0) {
      const sortedList = getSortedList({ filtered, direction, key, type });
      returnValue = getSearchedList({ search, searchableKeys, sortedList });
    }
    return returnValue;
  } else {
    return initialState.data;
  }
};

export const selectSearch = state =>
  state[reducerName] ? state[reducerName].search : initialState.search;

export const selectActiveSort = state => {
  if (state[reducerName]) {
    const { direction, key } = state[reducerName];
    return {
      direction,
      key
    };
  }
};

// ACTIONS //
export const INIT_TABLE = createActionName(reducerName, "INIT");
export const SET_SEARCH_VALUE = createActionName(
  reducerName,
  "SET_SEARCH_VALUE"
);
export const SET_SORT_TYPE = createActionName(reducerName, "SET_SORT_TYPE");

export const initTable = payload => ({
  type: INIT_TABLE,
  payload
});

export const setSearchValue = payload => ({
  type: SET_SEARCH_VALUE,
  payload
});

export const setSorting = payload => ({
  type: SET_SORT_TYPE,
  payload
});

export default function reducer(state = initialState, action = {}) {
  const { type, payload } = action;
  switch (type) {
    case INIT_TABLE:
      return {
        ...state,
        data: payload,
        filtered: payload
      };
    case SET_SEARCH_VALUE:
      return {
        ...state,
        search: payload
      };
    case SET_SORT_TYPE:
      return {
        ...state,
        direction: payload.direction,
        key: payload.key,
        type: payload.type
      };
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

import reducerRegistry from "../reducerRegistry";

import { SportsAPI, createActionName } from "../utils";

const initialState = {
  error: false,
  loading: false,
  errorData: {},
  data: {}
};

export const reducerName = "getLeagueStanding";

const url = "?action=get_standings";

export const getLeagueStandingData = leagueID => {
  return SportsAPI.get(url + `&league_id=${leagueID}`)
    .then(resopnse => {
      return resopnse;
    })
    .catch(error => {
      if (error.response) {
        return error.response;
      } else if (error.request) {
        return error.request;
      } else {
        return { status: 400, message: error.message };
      }
    });
};

// SELECTORS //
export const selectData = state => {
  return state[reducerName] ? state[reducerName].data : initialState.data;
};
export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;
export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const GET_STANDINGS_LOADING = createActionName(reducerName, "LOADING");
export const GET_STANDINGS_SUCCESS = createActionName(reducerName, "SUCCESS");
export const GET_STANDINGS_ERROR = createActionName(reducerName, "ERROR");
export const SET_ACTIVE_TEAM = createActionName(reducerName, "ACTIVE");

// ACTION CREATORS //
export const getLeagueStandingLoading = () => {
  return {
    type: GET_STANDINGS_LOADING
  };
};

export const getLeagueStandingSuccess = payload => {
  return {
    type: GET_STANDINGS_SUCCESS,
    payload
  };
};

export const getLeagueStandingError = error => {
  return {
    type: GET_STANDINGS_ERROR,
    error
  };
};

export const getLeagueStanding = leagueID => dispatch => {
  dispatch(getLeagueStandingLoading());
  getLeagueStandingData(leagueID).then(response => {
    if (response.status > 0 && response.status < 400) {
      const data = { [leagueID]: response.data };
      dispatch(getLeagueStandingSuccess(data));
    } else {
      dispatch(getLeagueStandingError(response));
    }
  });
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case GET_STANDINGS_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_STANDINGS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        data: { ...state.data, ...action.payload }
      };
    case GET_STANDINGS_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.error
      };
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

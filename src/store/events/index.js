import reducerRegistry from "../reducerRegistry";

import { SportsAPI, createActionName, formatDate } from "../utils";

const initialState = {
  error: false,
  loading: false,
  errorData: {},
  data: {}
};

export const reducerName = "getEventDetails";

const url = "?action=get_events";

export const getEventDetailsData = (leagueID, month) => {
  const { from, to } = formatDate(month);

  return SportsAPI.get(url + `&league_id=${leagueID}&from=${from}&to=${to}`)
    .then(resopnse => {
      return resopnse;
    })
    .catch(error => {
      if (error.response) {
        return error.response;
      } else if (error.request) {
        return error.request;
      } else {
        return { status: 400, message: error.message };
      }
    });
};

// SELECTORS //
export const selectData = state => {
  return state[reducerName] ? state[reducerName].data : initialState.data;
};
export const selectActiveTeam = state => {
  return state[reducerName]
    ? state[reducerName].selected
    : initialState.selected;
};
export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;
export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const GET_EVENTS_LOADING = createActionName(reducerName, "LOADING");
export const GET_EVENTS_SUCCESS = createActionName(reducerName, "SUCCESS");
export const GET_EVENTS_ERROR = createActionName(reducerName, "ERROR");
export const SET_ACTIVE_TEAM = createActionName(reducerName, "ACTIVE");

// ACTION CREATORS //
export const getEventDetailsLoading = () => {
  return {
    type: GET_EVENTS_LOADING
  };
};

export const getEventDetailsSuccess = payload => {
  return {
    type: GET_EVENTS_SUCCESS,
    payload
  };
};

export const getEventDetailsError = error => {
  return {
    type: GET_EVENTS_ERROR,
    error
  };
};

export const getEventDetails = (leagueID, month) => dispatch => {
  dispatch(getEventDetailsLoading());
  getEventDetailsData(leagueID, month).then(response => {
    if (response.status > 0 && response.status < 400) {
      const data = { [`${leagueID}-${month}`]: response.data };
      dispatch(getEventDetailsSuccess(data));
    } else {
      dispatch(getEventDetailsError(response));
    }
  });
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case GET_EVENTS_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_EVENTS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        data: { ...state.data, ...action.payload }
      };
    case GET_EVENTS_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.error
      };

    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

import React from "react";
import { StyleSheet, FlatList, Text, View, SafeAreaView } from "react-native";
import { connect } from "react-redux";

import {
  getEventDetails,
  selectErrorStatus,
  selectLoadingStatus,
  selectData
} from "store/events";

import Loader from "components/Loader";

import FadedHeading from "components/FadedHeading";

import FixtureCard from "components/Cards/FixtureCard";
import theme from "tlc/theme";
import EmptyState from "components/EmptyState";

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

class EventDetails extends React.Component {
  state = {
    month: 0,
    monthList: ["Jan", "Feb", "Mar", "Apr"]
  };

  handleClick = month => {
    const {
      getEventDetails,
      route: {
        params: { leagueID }
      },
      data
    } = this.props;

    if (month) {
      this.setState({ month });
      if (!data.hasOwnProperty(`${leagueID}-${month}`)) {
        getEventDetails(leagueID, month);
      }
    }
  };

  renderMonths = () => {
    const { monthList, month } = this.state;
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-around",
          alignItems: "center"
        }}
      >
        {monthList.map((name, index) => (
          <Text
            key={name + index}
            style={{
              fontFamily: theme.boldFont,
              padding: 20,
              borderBottomColor:
                month === index + 1 ? theme.primaryColor : theme.borderColor,
              borderBottomWidth: 3,
              color:
                month === index + 1 ? theme.primaryColor : theme.borderColor,
              borderRadius: 5
            }}
            onPress={() => this.handleClick(index + 1)}
          >
            {name}
          </Text>
        ))}
      </View>
    );
  };

  renderContent = () => {
    const {
      loading,
      data,
      route: {
        params: { leagueID }
      }
    } = this.props;

    const { month } = this.state;

    const matches = (month && data[`${leagueID}-${month}`]) || [];

    if (loading) {
      return (
        <View style={{ paddingVertical: 40, alignItems: "center" }}>
          <Loader />
        </View>
      );
    } else if (matches.length > 0) {
      return (
        <FlatList
          data={matches}
          renderItem={item => <FixtureCard {...item} />}
          keyExtractor={item => item.match_id}
        />
      );
    } else if (!month) {
      return <EmptyState text="Pick Month" icon="ios-calendar" />;
    } else {
      return <EmptyState />;
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <FadedHeading>2020</FadedHeading>
        {this.renderMonths()}
        {this.renderContent()}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  loading: selectLoadingStatus(state),
  data: selectData(state),
  error: selectErrorStatus(state)
});

const mapDispatchToProps = dispatch => ({
  getEventDetails: (leagueID, month) =>
    dispatch(getEventDetails(leagueID, month))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EventDetails);

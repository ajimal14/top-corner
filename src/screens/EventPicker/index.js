import React from "react";
import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  Image
} from "react-native";

import LeagueCard from "components/Cards/LeagueCard";
import FadedHeading from "components/FadedHeading";
import theme from "tlc/theme";
import { aspectSizer } from "utils";

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  button: {
    alignItems: "center",
    borderRadius: 5,
    width: "90%",
    alignSelf: "flex-start",
    marginTop: 20,
    shadowColor: "#e3e3e3",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.915,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: theme.clBlue,
    flexDirection: "row",
    justifyContent: "space-between",
    overflow: "hidden"
  }
});

class EventPicker extends React.Component {
  competetionList = [
    {
      country_id: "165",
      country_name: "Europe",
      league_id: "589",
      league_name: "Champions League",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/589_champions-league.png",
      country_logo: ""
    },
    {
      country_id: "165",
      country_name: "Europe",
      league_id: "590",
      league_name: "Europa League",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/590_europa-league.png",
      country_logo: ""
    },
    {
      country_id: "135",
      country_name: "Spain",
      league_id: "468",
      league_name: "LaLiga",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/468_laliga.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/135_spain.png"
    },
    {
      country_id: "41",
      country_name: "England",
      league_id: "8639",
      league_name: "FA Cup",
      league_season: "2019/2020",
      league_logo:
        "https://apiv2.apifootball.com/badges/logo_leagues/8639_fa-cup.png",
      country_logo:
        "https://apiv2.apifootball.com/badges/logo_country/41_england.png"
    }
  ];

  handleClick = leagueID => {
    const { navigation } = this.props;
    navigation.navigate("Event Details", {
      leagueID
    });
  };

  renderContent = () => {
    return this.competetionList.map(competetion => (
      <LeagueCard
        key={competetion.league_id}
        onPress={() => this.handleClick(competetion.league_id)}
        item={competetion}
      />
    ));
  };

  renderShowCL = () => {
    const { navigation } = this.props;
    return (
      <TouchableOpacity
        onPress={() => navigation.navigate("Champions League")}
        style={styles.button}
      >
        <Text
          style={{
            textAlign: "center",
            color: "#fff",
            textTransform: "uppercase",
            fontFamily: theme.boldFont,
            padding: 20,
            fontSize: aspectSizer(15)
          }}
        >
          Champions League Timeline
        </Text>
        <Image
          source={{
            uri:
              "https://apiv2.apifootball.com/badges/logo_leagues/589_champions-league.png"
          }}
          style={{
            height: 65,
            width: 65,
            backgroundColor: "#fff"
          }}
          resizeMode="center"
        />
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <FadedHeading>Pick Event</FadedHeading>
        <View
          style={{
            flexDirection: "row",
            flexWrap: "wrap",
            justifyContent: "space-around"
          }}
        >
          {this.renderContent()}
          {this.renderShowCL()}
        </View>
      </SafeAreaView>
    );
  }
}

export default EventPicker;
